var gulp = require('gulp'),
    imagemin = require('gulp-imagemin');
    
gulp.task('compress', function() {
    return gulp
        .src('dev/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/images'));
});
