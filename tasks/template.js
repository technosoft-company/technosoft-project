var gulp = require('gulp'),
    pug = require('gulp-pug');

// pug
function log(error) {
    console.log([
        '',
        "----------ERROR MESSAGE START----------",
        ("[" + error.name + " in " + error.plugin + "]"),
        error.message,
        "----------ERROR MESSAGE END----------",
        ''
    ].join('\n'));
    this.end();
}

gulp.task('pug', function() {
    gulp.src('dev/template/*.pug')
        .pipe(pug({
            pretty: true,
        })).on('error', log)
        .pipe(gulp.dest('public/'));
});
