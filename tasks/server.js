var gulp = require('gulp'),
    browserSync = require('browser-sync').create();

gulp.task('server', function() {
    browserSync.init({
        server: "public/",
        port: 80,
        open: false,
        localOnly: false,
        files: ["public/*.html", "public/css/*.css", "public/js/*.js", "public/icon/*.svg"]
    });
});
