var gulp = require('gulp'),
    fs = require('fs'),
    gutil = require('gulp-util'),
    ftp = require('vinyl-ftp');

var globalFolder = './public/**/*';
var ftpConf = JSON.parse(fs.readFileSync('ftp.json'));

function getFtpConnection() {
    return ftp.create({
        host: ftpConf.host,
        user: ftpConf.user,
        password: ftpConf.pass,
        parallel: 10,
        log: gutil.log
    });
}

gulp.task('deploy', function() {
    var conn = getFtpConnection();
    return gulp.src(globalFolder, { base: '.', buffer: false })

    .pipe(conn.newer(ftpConf.path)) // only upload newer files 
        .pipe(conn.dest(ftpConf.path));
});

gulp.task('deploy-watch', function() {
    var conn = getFtpConnection();
    gulp.watch(globalFolder)
        .on('change', function(event) {
            return gulp.src([event.path], { base: '.', buffer: false })
                .pipe(conn.newer(ftpConf.path)) // only upload newer files 
                .pipe(conn.dest(ftpConf.path))
        });
});
