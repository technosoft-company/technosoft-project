var gulp = require('gulp');

gulp.task('default', ['server', 'compress', 'pug', 'sass', 'css', 'js', 'svg'], function() {
	gulp.watch('dev/images/*', ['compress']);
    gulp.watch('dev/template/**/*.pug', ['pug']);
    gulp.watch('dev/scss/**/*.scss', ['sass']);
    gulp.watch('dev/css/*.css', ['css']);
    gulp.watch('dev/js/*.js', ['js']);
    gulp.watch('dev/icon/*.svg', ['svg']);
});